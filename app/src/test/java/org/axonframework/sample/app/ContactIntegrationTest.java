/*
 * Copyright (c) 2010. Axon Framework
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.axonframework.sample.app;

import javax.inject.Inject;
import org.axonframework.eventstore.EventStore;
import org.axonframework.repository.Repository;
import org.axonframework.sample.app.command.Contact;
import org.axonframework.sample.app.command.ContactCommandHandler;
import org.axonframework.sample.app.config.ApplicationContext;
import org.axonframework.sample.app.config.DatabaseContext;
import org.axonframework.sample.app.config.DatainitContext;
import org.axonframework.sample.app.query.ContactRepository;
import org.jboss.weld.environment.se.Weld;
import org.jboss.weld.environment.se.WeldContainer;
import org.junit.*;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
/*
import java.lang.annotation.Annotation;
import org.junit.runner.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
 */
import static org.junit.Assert.*;

/**
 * @author Allard Buijze
 */
//@RunWith(SpringJUnit4ClassRunner.class)
//@ContextConfiguration(locations = {"classpath:/META-INF/spring/application-context.xml",
//                                   "classpath:/META-INF/spring/database-context.xml"})
public class ContactIntegrationTest {

    @Inject /*@Autowired*/
    private ContactCommandHandler commandHandler;

    @Inject /*@Autowired*/
    private EventStore eventStore;

    @Inject /*@Autowired*/
    private ThreadPoolTaskExecutor taskExecutor;

    @Inject /*@Autowired*/
    private ContactRepository contactRepository;

    @Inject /*@Autowired*/
    private Repository<Contact> commandRepository;
    
    private Weld weld;
    
    @Before
    public void setup() {
    	weld = new Weld();
    	WeldContainer container = weld.initialize();
    	container.instance().select(DatabaseContext.class).get();
    	container.instance().select(ApplicationContext.class).get();
    	container.instance().select(DatainitContext.class).get();
    }
    
    @After 
    public void teardown() {
    	weld.shutdown();	
    }
/*
    @Test(timeout = 10000)
    public void testApplicationContext() throws InterruptedException {
        assertNotNull(commandHandler);
        assertNotNull(eventStore);
        assertNotNull(taskExecutor);
        assertNotNull(contactRepository);
        assertNotNull(commandRepository);
    }
*/
    @Test(timeout = 10000)
    public void testApplicationContext_commandHandler() throws InterruptedException {
        assertNotNull(commandHandler);
    }

    @Test(timeout = 10000)
    public void testApplicationContext_eventStore() throws InterruptedException {
        assertNotNull(eventStore);
    }

    @Test(timeout = 10000)
    public void testApplicationContext_taskExecutor() throws InterruptedException {
        assertNotNull(taskExecutor);
    }

    @Test(timeout = 10000)
    public void testApplicationContext_contactRepository() throws InterruptedException {
        assertNotNull(contactRepository);
    }

    @Test(timeout = 10000)
    public void testApplicationContext_commandRepository() throws InterruptedException {
        assertNotNull(commandRepository);
    }

}
