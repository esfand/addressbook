package org.axonframework.sample.app.config;

import java.util.LinkedList;
import java.util.List;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import org.axonframework.commandhandling.CommandBus;
import org.axonframework.commandhandling.CommandHandlerInterceptor;
import org.axonframework.commandhandling.SimpleCommandBus;
import org.axonframework.commandhandling.annotation.AnnotationCommandHandlerBeanPostProcessor;
import org.axonframework.commandhandling.interceptors.SpringTransactionalInterceptor;
import org.axonframework.eventhandling.EventBus;
import org.axonframework.eventhandling.SimpleEventBus;
import org.axonframework.eventhandling.annotation.AnnotationEventListenerBeanPostProcessor;
import org.axonframework.eventsourcing.EventCountSnapshotterTrigger;
import org.axonframework.eventsourcing.EventSourcingRepository;
import org.axonframework.eventsourcing.Snapshotter;
import org.axonframework.eventsourcing.SpringAggregateSnapshotter;
import org.axonframework.eventstore.SnapshotEventStore;
import org.axonframework.eventstore.jpa.JpaEventStore;
import org.axonframework.repository.Repository;
import org.axonframework.sample.app.command.Contact;
import org.axonframework.sample.app.command.ContactCommandHandler;
import org.axonframework.sample.app.command.ContactNameRepository;
import org.axonframework.sample.app.command.JpaContactNameRepository;
import org.axonframework.sample.app.query.ContactRepository;
import org.axonframework.sample.app.query.ContactRepositoryImpl;
//import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
//import org.springframework.transaction.PlatformTransactionManager;


@ApplicationScoped
public class ApplicationContext {
/*	
	@Produces ThreadPoolTaskExecutor                    taskExecutor;
	@Produces AnnotationEventListenerBeanPostProcessor  annotationEventListenerBeanPostProcessor;
	@Produces CommandBus                                commandBus;
	@Produces AnnotationCommandHandlerBeanPostProcessor annotationCommandHandlerBeanPostProcessor;
	@Produces EventBus                                  eventBus;
	@Produces Repository<Contact>                       contactRepository;
	@Produces SnapshotEventStore                        eventStore;
	@Produces ContactCommandHandler                     contactCommandHandler;
	@Produces Snapshotter                               snapshotter;
	@Produces ContactNameRepository                     contactNameRepository;
	
	@Inject   PlatformTransactionManager                transactionManager;
	@Inject   ContactRepository                         queryContactRepository;
*/	
	// org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor properties
	/*@Inject*/   int        corePoolSize;                    // 2
	/*@Inject*/   int        maxPoolSize;                     // 5
	/*@Inject*/   boolean    waitForJobsToCompleteOnShutdown; // true
	
	// org.axonframework.eventsourcing.EventCountSnapshotterTrigger property
	/*@Inject*/   int        trigger;                         // 5
/*
	public ApplicationContext() {
		corePoolSize                    = 2;
		maxPoolSize                     = 5;
		waitForJobsToCompleteOnShutdown = true;
		trigger                         = 5;
		
		taskExecutor                              = getTaskExecutor();
		annotationEventListenerBeanPostProcessor  = getAnnotationEventListenerBeanPostProcessor();
		commandBus                                = getCommandBus();
		annotationCommandHandlerBeanPostProcessor = getAnnotationCommandHandlerBeanPostProcessor();
		eventBus                                  = getEventBus();
		contactRepository                         = getContactRepository();
		eventStore                                = getEventStore();
		contactCommandHandler                     = getContactCommandHandler();
		snapshotter                               = getSnapshotter();
		contactNameRepository                     = getContactNameRepository();	
	}
	
	public ThreadPoolTaskExecutor getTaskExecutor() {
		ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
		executor.setCorePoolSize(corePoolSize);
		executor.setMaxPoolSize(maxPoolSize);
		executor.setWaitForTasksToCompleteOnShutdown(waitForJobsToCompleteOnShutdown);
		
		return executor;
	}
	
	public AnnotationEventListenerBeanPostProcessor getAnnotationEventListenerBeanPostProcessor() {
		AnnotationEventListenerBeanPostProcessor aelbpp = new AnnotationEventListenerBeanPostProcessor();
		aelbpp.setExecutor(taskExecutor);
		return aelbpp;
	}
	
	public CommandBus getCommandBus() {
		List<CommandHandlerInterceptor> interceptors = new LinkedList<CommandHandlerInterceptor>();
		
		SpringTransactionalInterceptor sti = new SpringTransactionalInterceptor();
		sti.setTransactionManager(transactionManager);
		interceptors.add(sti);
		
		SimpleCommandBus scb = new SimpleCommandBus();
		scb.setInterceptors(interceptors);
		return scb;
	}
	
	public AnnotationCommandHandlerBeanPostProcessor getAnnotationCommandHandlerBeanPostProcessor() {
		AnnotationCommandHandlerBeanPostProcessor achbpp = new AnnotationCommandHandlerBeanPostProcessor();
		achbpp.setCommandBus(commandBus);
		return achbpp;
	}
	
	public EventBus getEventBus() {
		SimpleEventBus seb = new SimpleEventBus();
		return seb;		
	}
	
	public ContactRepository getQueryContactRepository() {
		ContactRepositoryImpl cri = new ContactRepositoryImpl();
		return cri;
	}
	
	public SnapshotEventStore getEventStore() {
		JpaEventStore jes = new JpaEventStore();
		return jes;
	}
	
	public Snapshotter getSnapshotter() {
		SpringAggregateSnapshotter sas = new SpringAggregateSnapshotter();
		sas.setEventStore(eventStore);
		sas.setExecutor(taskExecutor);
		return sas;
	}
	
	public Repository<Contact> getContactRepository() {
		EventCountSnapshotterTrigger sst = new EventCountSnapshotterTrigger();
		sst.setSnapshotter(snapshotter);
		sst.setTrigger(trigger);
		
		EventSourcingRepository<Contact> esr = new EventSourcingRepository<Contact>(Contact.class);
		esr.setEventBus(eventBus);
		esr.setEventStore(eventStore);
		esr.setSnapshotterTrigger(sst);
		
		return esr;
	}
	
	public ContactCommandHandler getContactCommandHandler() {
		ContactCommandHandler cch = new ContactCommandHandler();
		cch.setRepository(contactRepository);
		cch.setContactRepository(queryContactRepository);
		cch.setContactNameRepository(contactNameRepository);

		return cch;
	}
	
	public ContactNameRepository getContactNameRepository() {
		JpaContactNameRepository jcnr = new JpaContactNameRepository();
		return jcnr;
	}
*/	
}



/*
    <context:annotation-config/>
    
    <bean id="taskExecutor" class="org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor">
        <property name="corePoolSize" value="2"/>
        <property name="maxPoolSize" value="5"/>
        <property name="waitForTasksToCompleteOnShutdown" value="true"/>
    </bean>

     <bean class="org.axonframework.eventhandling.annotation.AnnotationEventListenerBeanPostProcessor">
         <property name="executor" ref="taskExecutor" />
     </bean>

     <bean class="org.axonframework.commandhandling.annotation.AnnotationCommandHandlerBeanPostProcessor">
         <property name="commandBus" ref="commandBus" />
     </bean>

    <bean id="commandBus" class="org.axonframework.commandhandling.SimpleCommandBus">
         <property name="interceptors"> 
             <bean class="org.axonframework.commandhandling.interceptors.SpringTransactionalInterceptor">
                 <property name="transactionManager" ref="transactionManager"/> 
             </bean> 
         </property>
    </bean>

    <bean id="eventBus" class="org.axonframework.eventhandling.SimpleEventBus" />

    <bean id="contactRepository" class="org.axonframework.eventsourcing.EventSourcingRepository">
            <constructor-arg value="org.axonframework.sample.app.command.Contact"/>
            <property name="eventBus" ref="eventBus"/>
            <property name="eventStore" ref="eventStore"/>
            <property name="snapshotterTrigger">
                <bean class="org.axonframework.eventsourcing.EventCountSnapshotterTrigger">
                    <property name="snapshotter" ref="snapshotter"/>
                    <property name="trigger"     value="5"/>
                </bean>
            </property>
    </bean>

    <bean id="snapshotter" class="org.axonframework.eventsourcing.SpringAggregateSnapshotter">
        <property name="eventStore" ref="eventStore"/>
        <property name="executor"   ref="taskExecutor"/>
    </bean>

    <bean id="eventStore" class="org.axonframework.eventstore.jpa.JpaEventStore" />

    <bean id="contactCommandHandler" class="org.axonframework.sample.app.command.ContactCommandHandler">
        <property name="repository"            ref="contactRepository"/>
        <property name="contactRepository"     ref="queryContactRepository"/>
        <property name="contactNameRepository" ref="contactNameRepository"/>
    </bean>

    <bean id="contactNameRepository" class="org.axonframework.sample.app.command.JpaContactNameRepository"/>
 */

