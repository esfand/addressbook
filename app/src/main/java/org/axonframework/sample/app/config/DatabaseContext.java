package org.axonframework.sample.app.config;

import java.beans.PropertyVetoException;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import javax.transaction.TransactionManager;
import com.mchange.v2.c3p0.ComboPooledDataSource;
import org.axonframework.sample.app.query.AddressTableUpdater;
import org.axonframework.sample.app.query.ContactRepository;
import org.axonframework.sample.app.query.ContactRepositoryImpl;
/*
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;
*/
/*
import org.springframework.orm.jpa.support.PersistenceAnnotationBeanPostProcessor;
import org.springframework.orm.jpa.JpaDialect;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.Database;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;
*/

@ApplicationScoped
public class DatabaseContext {
	
	@Produces DataSource                             dataSource;
	@Produces EntityManagerFactory                   entityManagerFactory;
	@Produces TransactionManager                     transactionManager;
	@Produces AddressTableUpdater                    addressTableUpdater;
	@Produces ContactRepository                      queryContactRepository;
/*	
	@Produces PropertyPlaceholderConfigurer          propertyPlaceholderConfigurer;
	@Produces PersistenceAnnotationBeanPostProcessor persistenceAnnotationBeanPostProcessor;
*/
	/*@Inject*/   String  jdbcUrl;             // ${jdbc.driverclass}
	/*@Inject*/   String  driverClass;         // ${jdbc.url}
	/*@Inject*/   String  user;                // ${jdbc.username}
	/*@Inject*/   String  password;            // ${jdbc.password}
	/*@Inject*/   int     maxPoolSize;         // 15;
	/*@Inject*/   int     minPoolSize;         //  5;
	
	/*@Inject*/   String  persistenceUnitName; // "addresses"
	/*@Inject*/   boolean generateDdl;         // ${hibernate.sql.generateddl}
	/*@Inject*/   boolean showSql;             // ${hibernate.sql.show}
	
	/*@Inject*/   String  databasePlatform;    // "classpath:hsqldb.database.properties";
	

	public DatabaseContext() {
		jdbcUrl     = "jdbc:hsqldb:mem:addressbook"; // ${jdbc.driverclass}
		driverClass = "org.hsqldb.jdbcDriver";       // ${jdbc.url}
		user        = "sa";                          // ${jdbc.username}
		password    = null;                          // ${jdbc.password}
		maxPoolSize = 15;
		minPoolSize =  5;
		
		persistenceUnitName = "addresses"; // "addresses"
		generateDdl         = true;        // ${hibernate.sql.generateddl}
		showSql             = true;        // ${hibernate.sql.show}
		
		databasePlatform    = "classpath:hsqldb.database.properties";
		
		
//		dataSource                             = getDataSource();
        ComboPooledDataSource ds = new ComboPooledDataSource();
		try {
			ds.setDriverClass(driverClass);
		} 
		catch (PropertyVetoException e) {
			e.printStackTrace();
		}
		ds.setJdbcUrl(jdbcUrl);
		ds.setUser(user);
		ds.setPassword(password);
		ds.setMaxPoolSize(maxPoolSize);
		ds.setMinPoolSize(minPoolSize);
		dataSource = ds;
		
		entityManagerFactory                   = getEntityManagerFactory();
        transactionManager                     = getTransactionManager();
			
		addressTableUpdater                    = new AddressTableUpdater();
		queryContactRepository                 = new ContactRepositoryImpl();
/*		
		propertyPlaceholderConfigurer          = getPropertyPlaceholderConfigurer();
		persistenceAnnotationBeanPostProcessor = new PersistenceAnnotationBeanPostProcessor();
*/		
	}
/*
	public PropertyPlaceholderConfigurer getPropertyPlaceholderConfigurer() {
		PropertyPlaceholderConfigurer pphc = new PropertyPlaceholderConfigurer();
		pphc.setLocations(new Resource[]{new ClassPathResource(databasePlatform)});
		
		return pphc;
	}
*/	
	public DataSource getDataSource() {

        ComboPooledDataSource ds = new ComboPooledDataSource();
		try {
			ds.setDriverClass(driverClass);
		} 
		catch (PropertyVetoException e) {
			e.printStackTrace();
		}
		ds.setJdbcUrl(jdbcUrl);
		ds.setUser(user);
		ds.setPassword(password);
		ds.setMaxPoolSize(maxPoolSize);
		ds.setMinPoolSize(minPoolSize);
		
		return ds;
	}
	
	public EntityManagerFactory getEntityManagerFactory() {
/*		
		HibernateJpaVendorAdapter hjva = new HibernateJpaVendorAdapter();		
		hjva.setDatabasePlatform(databasePlatform);
		hjva.setGenerateDdl(generateDdl);
		hjva.setShowSql(showSql);
				
		LocalContainerEntityManagerFactoryBean lcemf = new LocalContainerEntityManagerFactoryBean();
		lcemf.setPersistenceUnitName(persistenceUnitName);
		lcemf.setJpaVendorAdapter(hjva);
		lcemf.setDataSource(dataSource);

		EntityManagerFactory emf = lcemf.getObject();
		return emf;
*/		
		assert(false);
		return null;
	}
	
	public TransactionManager getTransactionManager() {
//		JpaTransactionManager jtm = new JpaTransactionManager();
//		jtm.setEntityManagerFactory(entityManagerFactory);	
//		return jtm;
		
		assert(false);
		return null;
	}

}



/*
    <bean class="org.axonframework.sample.app.query.AddressTableUpdater"/>

    <bean id="queryContactRepository" class="org.axonframework.sample.app.query.ContactRepositoryImpl"/>

    <!-- Infrastructure configuration -->

    <bean class="org.springframework.beans.factory.config.PropertyPlaceholderConfigurer">
        <property name="locations" value="classpath:hsqldb.database.properties"/>
    </bean>

    <bean id="entityManagerFactory" class="org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean">
        <property name="persistenceUnitName" value="addresses"/>
        <property name="jpaVendorAdapter">
            <bean class="org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter">
                <property name="databasePlatform" value="${hibernate.sql.dialect}"/>
                <property name="generateDdl"      value="${hibernate.sql.generateddl}"/>
                <property name="showSql"          value="${hibernate.sql.show}"/>
            </bean>
        </property>
        <property name="dataSource" ref="dataSource"/>
    </bean>

    <tx:annotation-driven transaction-manager="transactionManager"/>

    <bean id="transactionManager" class="org.springframework.orm.jpa.JpaTransactionManager">
        <property name="entityManagerFactory" ref="entityManagerFactory"/>
    </bean>

    <bean class="org.springframework.orm.jpa.support.PersistenceAnnotationBeanPostProcessor"/>

    <bean id="dataSource" class="com.mchange.v2.c3p0.ComboPooledDataSource">
        <property name="driverClass" value="${jdbc.driverclass}"/>
        <property name="jdbcUrl"     value="${jdbc.url}"/>
        <property name="user"        value="${jdbc.username}"/>
        <property name="password"    value="${jdbc.password}"/>
        <property name="maxPoolSize" value="15"/>
        <property name="minPoolSize" value="5"/>
    </bean>
 */

