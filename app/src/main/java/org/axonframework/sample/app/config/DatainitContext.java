package org.axonframework.sample.app.config;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import org.axonframework.commandhandling.CommandBus;
import org.axonframework.sample.app.init.ContactGenerator;


@ApplicationScoped
public class DatainitContext {
	
	@Produces ContactGenerator contactGenerator;
	
	@Inject   CommandBus       commandBus;
	

	public DatainitContext() {
		contactGenerator = getContactGenerator();
	}

	public ContactGenerator getContactGenerator() {	
		ContactGenerator cg = new ContactGenerator(commandBus);
		return cg;
	}
}



/*
    <bean class="org.axonframework.sample.app.init.ContactGenerator">
        <constructor-arg ref="commandBus"/>
    </bean>
 */

